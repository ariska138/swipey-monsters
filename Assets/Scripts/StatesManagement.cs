﻿using UnityEngine;
using System.Collections;

public class StatesManagement : MonoBehaviour {
    public GameObject gameplay;
    public GameObject mainmenu;
    public GameObject result;
    public static int state = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {

        }
	}

    public void setState(int state)
    {
        switch (state)
        {
            case 0:
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(true);
                result.SetActive(false);
                break;
            case 1:
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(false);
                break;
            case 2:
                StatesManagement.state = state;
                gameplay.SetActive(true);
                mainmenu.SetActive(false);
                result.SetActive(true);
                break;

        }
    }
}
