﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioClip auNagaTerbang;
    [HideInInspector]
    public AudioSource asNagaTerbang;
    public AudioClip auNagaMati;
    [HideInInspector]
    public AudioSource asNagaMati;

    public AudioClip aumainmenu;
    [HideInInspector]
    public AudioSource asMainMenu;

    public AudioClip auScore;
    [HideInInspector]
    public AudioSource asScore;

    public AudioClip auButton;
    [HideInInspector]
    public AudioSource asButton;

    public AudioClip auGameplay;
    [HideInInspector]
    public AudioSource asGameplay;

    bool isMute;
	// Use this for initialization
	void Start () {
     //  asMainMenu = new AudioSource();
        asMainMenu = gameObject.AddComponent<AudioSource>();
        asMainMenu.clip = aumainmenu;
        asMainMenu.loop = true;
        

        asNagaTerbang = gameObject.AddComponent<AudioSource>();
        asNagaTerbang.clip = auNagaTerbang;


        asGameplay = gameObject.AddComponent<AudioSource>();
        asGameplay.clip = auGameplay;
        asGameplay.loop = true;

        asNagaMati = gameObject.AddComponent<AudioSource>();
        asNagaMati.clip = auNagaMati;

        asScore = gameObject.AddComponent<AudioSource>();
        asScore.clip = auScore;

        asButton = gameObject.AddComponent<AudioSource>();
        asButton.clip = auButton;

        // asGameplay.Play();
    }


	
	// Update is called once per frame
	void Update () {
	
	}

	public void Mute(){
//		isMute = !isMute;
////		AudioListener.volume = isMute ? 0 : 1;
//		PlayerPrefs.SetFloat ("MMute", AudioListener.volume);
	}
}
