﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.UI;
//using System;

public class Level1 : MonoBehaviour {

    public Text txLife;
    public Text txScore;
    public Text txBestScore;
    public Text txPlayer;
    public Text txTimeLogin;
    public Text txYourScore;

    SoundManager soundM;
    StatesManagement stateM;
    
    public GameObject[] pivot;
    public GameObject prefabEnemy;
    public GameObject prefabEnemy2;
    public GameObject prefabEnemy3;
    private GameObject[] goEnemy;
    public static bool isDestroyEnemy = false;
    public GameObject target;
    public GameObject particleDestroy;
//	public Text scoreText;
//	public GameObject anim;
//	private Animator scorePanel;
//	public GameObject tuts;

    //[SerializePrivateVariables]
    public static bool[] isShowEnemy;
    private float[] timerStanbyEnemy;
    private int[] condition;
    private float[] timerTouch;
    //MenuUI gui;

    public static int score = 0;
    public static int life = 5;
    public static int gameover = 0;
     int ibestScore = 0;
       int iplayer = 0;
	// Use this for initialization
	void Start () {
        ibestScore = PlayerPrefs.GetInt("best", 0);
        iplayer = PlayerPrefs.GetInt("player", 0);
        score = 0;
        life = 5;
        isShowEnemy = new bool[9];
        goEnemy = new GameObject[9];
        timerStanbyEnemy = new float[9];
        condition = new int[9];
        timerTouch = new float[9];

        stateM = GetComponent<StatesManagement>();
        stateM.setState(0);
        gameover = 0;
        
        soundM = GetComponent<SoundManager>();

        txTimeLogin.text = "";
        //spownEnemy();

    }

	//private void checkTutorial(){
	//	if (!PlayerPrefs.HasKey ("tutsDone")) {
	//		PlayerPrefs.SetString ("tutsDone", "true");
	//		tuts.SetActive (true);
	//	} else {
	//		tuts.SetActive (false);
	//	}
	//}

	//public void Finishing(){
	//	PlayerPrefs.SetInt("Score",score);
	//	gui.EnableScore (anim.GetComponent<Animator>());
	
	//}

    private void spownEnemy()
    {


        int iEnemy = (int)UnityEngine.Random.Range(0, 9);
        if (!isShowEnemy[iEnemy])
        {
            
            timerStanbyEnemy[iEnemy] = 0;
            isShowEnemy[iEnemy] = true;
            if(iEnemy == 0 || iEnemy == 3 || iEnemy == 6)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            else if (iEnemy == 1 || iEnemy == 4 || iEnemy == 7)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy2, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 270);
            }
            else if (iEnemy == 2 || iEnemy == 5 || iEnemy == 8)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy3, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 180);
            }
                
            goEnemy[iEnemy].transform.localScale = new Vector3(0, 0, 1);
            goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown",true);
            //goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown", false);
            goEnemy[iEnemy].name = "enemy_" + iEnemy;
            if(!soundM.asNagaTerbang.isPlaying)
            soundM.asNagaTerbang.Play();
            condition[iEnemy] = 1;
            
        }
    }



    private void moveToEgg(int idx)
    {
        if (isShowEnemy[idx] && condition[idx] != 2)
        {
            timerStanbyEnemy[idx] += Time.deltaTime;
            if (timerStanbyEnemy[idx] > 1)
            {
               // if (!soundM.asNagaTerbang.isPlaying)
              //  {
                   
             //   }
                float dX = target.transform.position.x - goEnemy[idx].transform.position.x;
                float dY = target.transform.position.y - goEnemy[idx].transform.position.y;

                float rad = Mathf.Atan2(dY, dX);
                float deg = rad * Mathf.Rad2Deg;

                goEnemy[idx].transform.localEulerAngles = new Vector3(0, 0, deg);

                // if (!goEnemy[idx].GetComponent<Animator>().GetBool("isFly"))
                // {
                goEnemy[idx].GetComponent<Animator>().SetBool("isFly", true);
              //  }

                if (goEnemy[idx] != null)
                {
                    goEnemy[idx].transform.position = Vector3.MoveTowards(goEnemy[idx].transform.position, target.transform.position, Time.deltaTime * 5f);
                    
                }
            }
        }
    }

    float timerShowEnemy = 0;

    float timerLogin = 0;
    int countLogin = 5;

	// Update is called once per frame
	void Update () {
        //scoreText.text = score.ToString();

        //if (Input.GetKeyDown (KeyCode.Escape)) {
        //	if (anim.GetComponent<Animator> ().GetBool ("isPaused")) {
        //		gui.DisablePause (anim.GetComponent<Animator> ());
        //	}else if(!anim.GetComponent<Animator> ().GetBool ("isPaused")){
        //		gui.EnablePause (anim.GetComponent<Animator> ());
        //	}

        //}

        if(gameover == 0)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (!soundM.asMainMenu.isPlaying) soundM.asMainMenu.Play();
            if (HandsViewer.handSensor[0].visible && HandsViewer.handSensor[1].visible)
            {
                timerLogin += Time.deltaTime;
                if(timerLogin > 1)
                {
                    timerLogin = 0;
                    countLogin--;
                    if(!soundM.asButton.isPlaying)
                    soundM.asButton.Play();
                    txTimeLogin.text = countLogin.ToString();
                }

               
                if(countLogin < 1)
                {
                    gameover = 1;
                    stateM.setState(1);
                    soundM.asMainMenu.Stop();
                }
            }
            else
            {
                countLogin = 5;
                txTimeLogin.text = "";
               timerLogin = 0;
            }
        }else
        if (gameover == 1)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.LoadLevel(1);
            }

            if (!soundM.asGameplay.isPlaying)
            {
                soundM.asGameplay.Play();
            }
            timerShowEnemy += Time.deltaTime;
            if (timerShowEnemy > 0.5f)
            {
                spownEnemy();
                timerShowEnemy = 0;
            }

            moveToEgg(0);
            moveToEgg(1);
            moveToEgg(2);
            moveToEgg(3);
            moveToEgg(4);
            moveToEgg(5);
            moveToEgg(6);
            moveToEgg(7);
            moveToEgg(8);


            TouchEnemy();

            TouchRealsense();



        }
        else if (gameover == 2)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.LoadLevel(1);
            }
            timerShowEnemy += Time.deltaTime;
            if (timerShowEnemy > 1)
            {
                timerShowEnemy = 0;
                gameover = 3;
                soundM.asScore.Play();
                stateM.setState(2);
                iplayer++;
                if(score > ibestScore)
                {
                    ibestScore = score;
                }
                txYourScore.text = score.ToString();
                PlayerPrefs.SetInt("best", ibestScore);
                PlayerPrefs.SetInt("player", iplayer);
                txBestScore.text = ibestScore.ToString();
                txPlayer.text = iplayer.ToString();
            }
            if (isDestroyEnemy)
            {
                isDestroyEnemy = false;
                for (int n = 0; n < 9; n++)
                {
                    if (goEnemy[n] != null)
                    {
                        condition[n] = 2;
                    }
                }
            }
        }
        else if (gameover == 3)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.LoadLevel(1);
            }
            timerShowEnemy += Time.deltaTime;
            if (timerShowEnemy > 3)
            {
                timerShowEnemy = 0;
                gameover = 4;
                stateM.setState(0);
            }
        }else if(gameover == 4)
        {
            gameover = 0;
            Application.LoadLevel(1);
        }

        RespondDestroyEnemy(0);
        RespondDestroyEnemy(1);
        RespondDestroyEnemy(2);
        RespondDestroyEnemy(3);
        RespondDestroyEnemy(4);
        RespondDestroyEnemy(5);
        RespondDestroyEnemy(6);
        RespondDestroyEnemy(7);
        RespondDestroyEnemy(8);
    }

    float timerShowResult = 0;
    void FixedUpdate()
    {
        txLife.text = "Life: " + life;
        txScore.text = "Score: " + score;
    }
    private void TouchRealsense()
    {
      //  if (HandsViewer.isTouch)
      //  {
          //  HandsViewer.isTouch = false;
            if (HandsViewer.handSensor[0].visible)
            {
                Vector2 v2 = new Vector2(HandsViewer.handSensor[0].pos.x, HandsViewer.handSensor[0].pos.y);
                Collider2D c2d = Physics2D.OverlapPoint(v2);
                if (c2d != null)
                {
                
                    PositiveTouch(c2d);
                    Debug.Log("Tap 0");
                   // HandsViewer.handSensor[0].status = "";
                   // HandsViewer.handSensor[1].status = "";
                }
            }

            
            if (HandsViewer.handSensor[1].visible)
            {
                Vector2 v2 = new Vector2(HandsViewer.handSensor[1].pos.x, HandsViewer.handSensor[1].pos.y);
                Collider2D c2d = Physics2D.OverlapPoint(v2);
                if (c2d != null)
                {

                PositiveTouch(c2d);
                    Debug.Log("Tap 1");
                   // HandsViewer.handSensor[0].status = "";
                   // HandsViewer.handSensor[1].status = "";
                }
            }

        //     HandsViewer.handSensor[0].status = "";
         //    HandsViewer.handSensor[1].status = "";

       // }


    }

    private void PositiveTouch(Collider2D c2d)
    {
        if (c2d.name.Length > 6)
        {
            string idxEnemy = c2d.name.Substring(6, 1);
            try {
                int idx = int.Parse(idxEnemy);
                c2d.gameObject.GetComponent<Animator>().SetBool("isDead", true);
                condition[idx] = 2;
            } catch(FormatException)
            {

            }
            
            //isShowEnemy[idx] = false;
            //	score++;
          
        }
        // Destroy(c2d.gameObject);
    }

    private void RespondDestroyEnemy(int idx)
    {
        if(condition[idx] == 2)
        {
            timerTouch[idx] += Time.deltaTime;
            if(timerTouch[idx] > 0.5f)
            {
                soundM.asNagaMati.Stop();
                soundM.asNagaMati.Play();
                timerTouch[idx] = 0;
                condition[idx] = 0;
                isShowEnemy[idx] = false;
               GameObject ps = (GameObject) Instantiate(particleDestroy, goEnemy[idx].transform.position, goEnemy[idx].transform.rotation);
                Destroy(ps, 1);
                Destroy(goEnemy[idx]);
                score += 125;
                // Debug.Log("KLSHFKLSHDKLFJS");
            }
        }
    }

    private void TouchEnemy()
	{	
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch currentTouch = Input.GetTouch(i);
                if (currentTouch.phase == TouchPhase.Began)
                {
                    Vector2 v2 = new Vector2(Camera.main.ScreenToWorldPoint(currentTouch.position).x, Camera.main.ScreenToWorldPoint(currentTouch.position).y);
                    Collider2D c2d = Physics2D.OverlapPoint(v2);

                    if (c2d != null)
                    {
                        Debug.Log("ketek");
                        PositiveTouch(c2d);
                    }
                }
            }
        }
    }
}
